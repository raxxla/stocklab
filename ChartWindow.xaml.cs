﻿#region S# License
/******************************************************************************************
NOTICE!!!  This program and source code is owned and licensed by
StockSharp, LLC, www.stocksharp.com
Viewing or use of this code requires your acceptance of the license
agreement found at https://github.com/StockSharp/StockSharp/blob/master/LICENSE
Removal of this comment is a violation of the license agreement.

Project: SampleQuik.SampleQuikPublic
File: ChartWindow.xaml.cs
Created: 2015, 11, 11, 2:32 PM

Copyright 2010 by StockSharp, LLC
*******************************************************************************************/
#endregion S# License
namespace SampleQuik
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Windows.Media;

	using StockSharp.Algo;
	using StockSharp.Algo.Indicators;
	using StockSharp.Algo.Candles;
	using StockSharp.BusinessEntities;
	using StockSharp.Quik;
	using StockSharp.Localization;
	using StockSharp.Logging;
	using StockSharp.Algo.Strategies;
	using StockSharp.Xaml.Charting;

	using SampleQuik.Script;

	partial class ChartWindow
	{
		private readonly QuikTrader _trader;
		private readonly CandleSeries _candleSeries;
		private readonly ChartCandleElement _candleElem;

		List<Candle> _candles = new List<Candle>();
		DateTime _lastCandleTime;
		bool _realtime = false;

		public List<Candle> Candles => _candles;
		public Candle LastCandle => _candles.Count > 0 ? _candles[_candles.Count - 1] : null;
		public Candle Get(int barsAgo = 0) => _candles[_candles.Count - 1 - barsAgo];

		public ChartWindow(CandleSeries candleSeries, DateTimeOffset? from = null, DateTimeOffset? to = null)
		{
			InitializeComponent();

			_candleSeries = candleSeries ?? throw new ArgumentNullException(nameof(candleSeries));
			_trader = MainWindow.Instance.Trader;

			Chart.ChartTheme = ChartThemes.ExpressionDark;

			var area = new ChartArea();
			Chart.Areas.Add(area);

			_candleElem = new ChartCandleElement
			{
				AntiAliasing = false, 
				UpFillColor = Colors.White,
				UpBorderColor = Colors.Black,
				DownFillColor = Colors.Black,
				DownBorderColor = Colors.Black,
			};

			area.Elements.Add(_candleElem);

			_trader.CandleSeriesProcessing += ProcessNewCandle;
			_trader.SubscribeCandles(_candleSeries, from, to);

			Title = candleSeries.ToString();

			TsLabStrategy strategy = null;
			TsLabStrategy[] instruments = null;
			Candle[] candles = null;
			IIndicator[] indicators = null;

			//_trader.WhenCandlesChanged(_candleSeries)
			//	.And(strategy.WhenPositionOpened())
			//	.Do( () => TryToClosePosition(candles, indicators, instruments))
			//	.Once()
			//	.Apply(this);

			//_trader.WhenCandlesChanged(_candleSeries)
			//	.And(strategy.WhenPositionClosed())
			//	.Do(() => TryToOpenPosition(candles, indicators, instruments))
			//	.Once()
			//	.Apply(this);
		}

		private void ProcessNewCandle(CandleSeries series, Candle candle)
		{
			if (series != _candleSeries)
				return;

			//var cds = _trader.GetCandles<TimeFrameCandle>(_candleSeries);

			//StockSharp.Algo.Indicators.ExponentialMovingAverage ema;
			//var iv = new StockSharp.Algo.Indicators.CandleIndicatorValue(
			//StockSharp.Algo.Indicators.IIndicator
			//ema.Process(0.0);

			var openTime = candle.OpenTime.DateTime;
			var closeTime = candle.CloseTime.DateTime;

			if (openTime == _lastCandleTime)
			{
				_candles[_candles.Count - 1] = candle;
				_candles.Add(candle);
			}

			if (openTime > _lastCandleTime)
			{
				_lastCandleTime = openTime;

				_candles.Add(candle);
			}

			Chart.Draw(_candleElem, candle);

			if (_realtime)
				ExecuteScript();
		}

		private void ExecuteScript()
		{
			//StockSharp.Algo.Indicators.SimpleMovingAverage sma = new StockSharp.Algo.Indicators.SimpleMovingAverage();
			
			//_script.Execute(this, _trader);
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_trader.UnSubscribeCandles(_candleSeries);
			_trader.CandleSeriesProcessing -= ProcessNewCandle;

			base.OnClosing(e);
		}

		private void Chart_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (_realtime)
				return;

			if (e.Key == System.Windows.Input.Key.Space)
			{
				_realtime = true;

				Title += " (realtime)";
			}
		}
	}
}