﻿using System.Globalization;

namespace Ast
{
    public class Parameter
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
        public string Value { get; set; }

		public double DoubleValue => double.Parse(Value, NumberStyles.Any, NumberFormatInfo.InvariantInfo);
		public int IntValue => (int)DoubleValue;
		public decimal DecimalValue => (decimal)DoubleValue;
	}
}
