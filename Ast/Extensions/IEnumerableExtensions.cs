﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Ast.Extensions
{
    public static class IEnumerableExtensions
    {
        public static string TryGetAttributeValue(this IEnumerable<XAttribute> collection, string key)
        {
            var attribute = collection.FirstOrDefault(p => p.Name.LocalName == key);
            return attribute != null ? attribute.Value : null;
        }
    }
}
