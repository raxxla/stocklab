﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Ast
{
    public static class HandlerFactory
    {
        public static IEnumerable<Handler> Create(string fileName, IXmlParser parser, out IEnumerable<XmlLink> xmlLinks)
        {
            var document = XDocument.Load(fileName);
            var handlers = parser.GetHandlers(document);
            xmlLinks = parser.GetLinks(document);

            return MapHandlers(handlers, xmlLinks);
        }

		public static IEnumerable<Handler> Create(XDocument document, IXmlParser parser, out IEnumerable<XmlLink> xmlLinks)
		{
			var handlers = parser.GetHandlers(document);
			xmlLinks = parser.GetLinks(document);

			return MapHandlers(handlers, xmlLinks);
		}

		private static IEnumerable<Handler> MapHandlers(IDictionary<string, Handler> handlers, IEnumerable<XmlLink> xmlLinks)
        {
            var result = new List<Handler>();

            foreach (var h in handlers)
            {
                var handler = h.Value;
                // Children
                var links = xmlLinks.Where(p => p.From == h.Key)
                    .ToList();

                foreach (var l in links)
                {
                    handler.Links.Add(new Link
                    {
                        From = handlers[l.From],
                        To = handlers[l.To],
                        FromPort = l.FromPort,
                        ToPort = l.ToPort,
                        ToPortNum = l.ToPortNum
                    });
                }

                // Parents
                links = xmlLinks.Where(p => p.To == h.Key)
                    .ToList();

                foreach (var l in links)
                {
                    handler.Links.Add(new Link
                    {
                        From = handlers[l.From],
                        To = handlers[l.To],
                        FromPort = l.FromPort,
                        ToPort = l.ToPort,
                        ToPortNum = l.ToPortNum
                    });
                }

                result.Add(handler);
            }

            SetInputPorts(result);
            SetOutputPorts(result);

            return result;
        }

        private static void SetInputPorts(IEnumerable<Handler> handlers)
        {
            foreach (var h in handlers)
            {
                var inputLinks = h.InputLinks.ToList();

                if (h.Inputs.Count > 0)
                {
                    for (int i = 0; i < h.Inputs.Count; i++)
                    {
                        var input = h.Inputs[i];
                        var link = inputLinks.FirstOrDefault(p => p.From.Key == input);
                        inputLinks.Remove(link);
                        inputLinks.Insert(i, link);
                    }
                }

                for (int i = 0; i < inputLinks.Count; i++)
                {
                    var link = inputLinks[i];
                    link.FromPort = link.FromPort ?? "Out";
                    link.ToPort = link.ToPort ?? (i + 1).ToString();
                    link.ToPortNum = link.ToPortNum ?? i;
                }
            }
        }

        private static void SetOutputPorts(IEnumerable<Handler> handlers)
        {
            foreach (var h in handlers)
            {
                var outputLinks = h.OutputLinks.ToList();

                for (int i = 0; i < outputLinks.Count; i++)
                {
                    var link = outputLinks[i].To.InputLinks.FirstOrDefault(p => p.From.Key == h.Key);
                    outputLinks[i].ToPort = link.ToPort;
                    outputLinks[i].ToPortNum = link.ToPortNum;
                }
            }
        }
    }
}