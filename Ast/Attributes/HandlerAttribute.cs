﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ast.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class HandlerAttribute : Attribute
    {
        public string TypeName { get; set; }

        public HandlerAttribute(string typeName)
        {
            TypeName = typeName;
        }
    }
}