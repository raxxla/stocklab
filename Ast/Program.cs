﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Ast
{

	class Program
    {
		static List<string> indicators = new List<string>();

		public static StringBuilder _code = new StringBuilder();

		static Program()
		{
			indicators.Add("TSLab.Script.Handlers.SMA");
			indicators.Add("TSLab.Script.Handlers.EMA");
			indicators.Add("TSLab.Script.Handlers.Highest");
			indicators.Add("TSLab.Script.Handlers.Lowest");
			indicators.Add("TSLab.Script.Handlers.SummFor");
			indicators.Add("TSLab.Script.Handlers.Volatility");
			indicators.Add("TSLab.Script.Handlers.StdDev");
		}

		static void Main2(string[] args)
        {
			var fileName = @"C:\temp_tslab\test1.xml";
			var parser = XmlParser.FindParser(fileName, out var doc);
			var handlers = HandlerFactory.Create(doc, parser, out var xmlLinks).ToArray();

			_code.AppendLine("using StockSharp.Algo;");
			_code.AppendLine();
			_code.AppendLine("namespace SampleQuik");
			_code.AppendLine("{");

			_code.AppendLine("\tpublic class TsLabScript");
			_code.AppendLine("\t{");

			var blocks = new List<Handler>();

			foreach(var block in handlers)
			{
				var typeName = block.ShortTypeName;

				if (typeName != null && block.CodeName != null)
				{
					//block.DeclCode = $"\t\tprivate {typeName} {block.CodeName}_handler = new {typeName}();";

					//_code.AppendLine(block.DeclCode);

					blocks.Add(block);
				}
			}

			_code.AppendLine();
			_code.AppendLine( "\t\tpublic void Execute(TSLab.Script.Handlers.IContext context, Connector connector)");
			_code.AppendLine("\t\t{");

			foreach (var handler in handlers)
			{
				if (handler.ShortTypeName == null)
					continue;

				foreach (var p in handler.Parameters)
				{
					_code.AppendLine($"\t\t\t{handler.CodeName}_handler.{p.Name} = {p.Value};");
				}
			}

			foreach (var handler in handlers)
			{
				if (handler.OutputLinks.Count() == 0)
					ProcessHandler(handler);
			}

			_code.AppendLine("\t\t}");

			_code.AppendLine("\t}");

			_code.AppendLine("}");

			var code = _code.ToString();

			File.WriteAllText("TsLabScript.cs", code);
		}

		private static void ProcessHandler(Handler handler)
		{
			if (handler.ValueRefExp != null)
				return;

			foreach (var parent in handler.Parents)
				ProcessHandler(parent);

			if (handler.ShortTypeName == null || handler.CodeName == null)
			{
				//if (handler.Category == "SECURITY" && handler.Value == null)
				//{
				//	handler.ValueCode = $"\t\t\tvar {handler.CodeName} = context;";

				//	_code.AppendLine(handler.ValueCode);
				//	_code.AppendLine();
				//}

				return;
			}

			if (indicators.Contains(handler.ShortTypeName))
			{
				GenerateIndicatorCall(handler);

				return;
			}

			var args = string.Empty;

			foreach (var a in handler.Parents)
			{
				if (args.Length == 0)
					args += a.CodeName;
				else
					args += $", {a.CodeName}";
			}

			//handler.ValueCode = $"\t\t\tvar {handler.CodeName} = {handler.CodeName}_handler.Execute({args});";

			//_code.AppendLine(handler.ValueCode);
		}

		private static void GenerateIndicatorCall(Handler indic)
		{
			var args = string.Empty;

			foreach (var a in indic.Parents)
			{
				if (args.Length == 0)
					args += $"{a.CodeName}_handler";
				else
					args += $", {a.CodeName}_handler";
			}

			//indic.ValueCode = $"\t\t\tvar {indic.CodeName} = {indic.CodeName}_handler.Execute(context, {args});";

			//_code.AppendLine(indic.ValueCode);
		}

		private static void CompareHandlers(Handler[] handlers1, Handler[] handlers2)
	    {
			if (handlers1.Length != handlers2.Length)
				throw new InvalidOperationException();

			var i = 0;
			foreach (var h1 in handlers1)
			{
				var h2 = handlers2[i];

				if (h1.GetType() != h2.GetType())
					throw new InvalidOperationException();

				if (h1.Key != h2.Key)
					throw new InvalidOperationException();

				//if (h1.Category != h2.Category)
				//	throw new InvalidOperationException();

				//if (h1.CodeName != h2.CodeName)
				//	throw new InvalidOperationException();

				//if (h1.Location != h2.Location)
				//	throw new InvalidOperationException();

				//if (h1.TypeName != h2.TypeName)
				//	throw new InvalidOperationException();

				//if (h1.Compacted != h2.Compacted)
				//	throw new InvalidOperationException();

				//if (h1.IsParametersVisible != h2.IsParametersVisible)
				//	throw new InvalidOperationException();

				//if (h1.NotTradable != h2.NotTradable)
				//	throw new InvalidOperationException();

				CompareHandlers(h1.Children.ToArray(), h2.Children.ToArray());

				//CompareHandlers(h1.Parents.ToArray(), h2.Parents.ToArray());

				if (h1.Parameters.Count != h2.Parameters.Count)
					throw new InvalidOperationException();

				var j = 0;
				foreach (var p1 in h1.Parameters)
				{
					var p2 = h2.Parameters[j];

					if (p1.Name != p2.Name)
						throw new InvalidOperationException();

					if (p1.Value?.ToLowerInvariant() != p2.Value?.ToLowerInvariant())
						throw new InvalidOperationException();

					if (p1.TypeName == "int")
						p1.TypeName = "Int32";

					if (p1.TypeName == "bool")
						p1.TypeName = "Boolean";

					if (p1.TypeName != p2.TypeName)
						throw new InvalidOperationException();

					j++;
				}

				//if (h1.Inputs.Count != h2.Inputs.Count)
				//	throw new InvalidOperationException();

				//j = 0;

				//foreach (var i1 in h1.Inputs)
				//{
				//	if (i1 != h2.Inputs[j])
				//		throw new InvalidOperationException();

				//	j++;
				//}

				CompareLinks(h1.InputLinks.ToArray(), h2.InputLinks.ToArray());

				CompareLinks(h1.OutputLinks.ToArray(), h2.OutputLinks.ToArray());

				i++;
			}
		}

	    private static void CompareLinks(Link[] links1, Link[] links2)
	    {
			if (links1.Length != links2.Length)
				throw new InvalidOperationException();

			var i = 0;
		    foreach (var l1 in links1)
		    {
			    var l2 = links2[i];

				if (l1.From.Key != l2.From.Key)
					throw new InvalidOperationException();

				//if (l1.FromPort != l2.FromPort)
				//	throw new InvalidOperationException();

				if (l1.To.Key != l2.To.Key)
					throw new InvalidOperationException();

				//if (l1.ToPort != l2.ToPort)
				//	throw new InvalidOperationException();

				//if (l1.ToPortNum != l2.ToPortNum)
				//	throw new InvalidOperationException();

				i++;
		    }
		}
    }
}