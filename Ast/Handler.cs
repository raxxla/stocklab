﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ast
{
    public class Handler
    {
		public int DataSourceIndex = -1;

		public string Mapping { get; set; }

		public object SSharpIndicator;

		public Expression InstrumentRefExp { get; set; }
		public Expression InstrumentInitExp { get; set; }

		public Expression IndicRefExp { get; set; }
		public Expression IndicInitExp { get; set; }

		public Expression ValueRefExp { get; set; }
		public Expression ValueInitExp { get; set; }

		public Expression Selector { get; set; }

		public string Key { get; set; }
        public string Category { get; set; }
        public string Location { get; set; }
        public string TypeName { get; set; }
		public string HandlerTypeName { get; set; }
		public bool IsParametersVisible { get; set; }
        public string CodeName { get; set; }
        public bool OnlyValueHandlersCanUsed { get; set; }
        public bool Compacted { get; set; }
        public bool NotTradable { get; set; }
        public IList<Parameter> Parameters { get; set; }
        public IList<Link> Links { get; set; }
        public IList<string> Inputs { get; set; }

		public string DataSourceName
		{
			get
			{
				if (DataSourceIndex >= 0)
					return Key;

				var parents = Links.Where(p => p.To.Key == Key).Select(p => p.From).ToList();

				foreach (var input in parents)
				{
					var result = input.DataSourceName;

					if (result != null)
						return result;
				}

				return null;
			}
		}

		public Expression ClosestSelector
		{
			get
			{
				if (Selector != null)
					return Selector;

				var parents = Links.Where(p => p.To.Key == Key).Select(p => p.From).ToList();

				foreach (var input in parents)
				{
					var result = input.ClosestSelector;

					if (result != null)
						return result;
				}

				return null;
			}
		}

		public Handler ClosestSelectorHandler
		{
			get
			{
				if (Selector != null)
					return this;

				var parents = Links.Where(p => p.To.Key == Key).Select(p => p.From).ToList();

				foreach (var input in parents)
				{
					var result = input.ClosestSelectorHandler;

					if (result != null)
						return result;
				}

				return null;
			}
		}

		public List<Handler> Children
        {
            get
            {
                return Links.Where(p => p.From.Key == Key)
                    .Select(p => p.To)
                    .ToList();
            }
        }

        public List<Handler> Parents
        {
            get
            {
                return Links.Where(p => p.To.Key == Key)
                    .Select(p => p.From)
                    .ToList();
            }
        }

        public IEnumerable<Link> InputLinks
        {
            get
            {
                return Links.Where(p => p.From.Key != Key)
                    .ToList();
            }
        }

		public IEnumerable<Link> OutputLinks => Links.Where(p => p.From.Key == Key).ToList();

		public Handler()
        {
            Links = new List<Link>();
            Parameters = new List<Parameter>();
            Inputs = new List<string>();
        }

		public StockSharp.Algo.Indicators.IIndicator CreateIndicator() => (SSharpIndicator as SampleQuik.Script.StockSharpIndicator).CreateInstance(this);

		public double GetParameterValue(int paramIndex, double defaultValue)
		{
			var len = Parameters?.Count ?? 0;

			if (paramIndex >= len)
				return defaultValue;

			return Parameters[paramIndex].DoubleValue;
		}
		public decimal GetParameterValue(int paramIndex, decimal defaultValue)
		{
			var len = Parameters?.Count ?? 0;

			if (paramIndex >= len)
				return defaultValue;

			return new decimal(Parameters[paramIndex].DoubleValue);
		}
		public int GetParameterValue(int paramIndex, int defaultValue)
		{
			var len = Parameters?.Count ?? 0;

			if (paramIndex >= len)
				return defaultValue;

			return Parameters[paramIndex].IntValue;
		}

		public override string ToString() => $"[{Category ?? Key}]:{CodeName} ({InputLinks.Count()} -> {OutputLinks.Count()}) {TypeName ?? HandlerTypeName}";

		public string ShortTypeName => HandlerTypeName != null ? HandlerTypeName.Substring(0, HandlerTypeName.IndexOf(',')) : null;
	}
}