﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Ast.Extensions;

namespace Ast.Parsers
{
    public class XmlParser1 : IXmlParser
    {
        public IDictionary<string, Handler> GetHandlers(XDocument document)
        {
            var xmlNodes = GetXmlNodes(document);

            if (!xmlNodes.Any())
                throw new Exception();

            return CreateHandlers(xmlNodes);
        }

        public IEnumerable<XmlLink> GetLinks(XDocument document)
        {
            var xmlNodes = GetXmlNodes(document);
            var links = new[] { "Link", "GraphLink" };

            if (!xmlNodes.Any())
                throw new Exception();

            foreach (var node in xmlNodes)
            {
                if (!links.Contains(node.Name.ToString()))
                    continue;

                yield return new XmlLink
                {
                    From = node.Attribute("From").Value,
                    To = node.Attribute("To").Value,
                    FromPort = node.Attribute("FromPort").Value,
                    ToPort = node.Attribute("ToPort").Value,
                    ToPortNum = node.Attribute("ToPortNum") != null ? 
                        (int?)Convert.ToInt32(node.Attribute("ToPortNum").Value) : 
                        null
                };
            }
        }

        private IEnumerable<XElement> GetXmlNodes(XDocument document)
        {
            return document.Element("GraphDataBase")
                .Element("EditData")
                .Element("ViewModel")
                .Elements("Model")
                .Elements();
        }

		public IEnumerable<XElement> GetXmlMappingNodes(XDocument document)
		{
			return document.Element("GraphDataBase")
				.Element("Mappings")
				.Element("Sources")
				.Elements();
		}

		private IDictionary<string, Handler> CreateHandlers(IEnumerable<XElement> nodes)
        {
            var result = new Dictionary<string, Handler>();
            var blocks = new[] { "Block", "Pane" };

            foreach (var xmlNode in nodes)
            {
                if (!blocks.Contains(xmlNode.Name.ToString()))
                    continue;

                var handler = CreateHandler(xmlNode);
                SetHandlerProperties(handler, xmlNode);
                result.Add(handler.Key, handler);
            }

            return result;
        }

        private Handler CreateHandler(XElement node)
        {
            var handlerTypeName = GetHandlerTypeNameAttribute(node);
            var typeName = node.Attributes().TryGetAttributeValue("TypeName");

            //if (handlerTypeName != null)
            //{
            //    var handlerTypeName2 = GetTypeName(handlerTypeName);
            //    var types = Assembly.GetExecutingAssembly().GetTypes();

            //    foreach (var t in types)
            //    {
            //        if (t.FullName.Contains(handlerTypeName2))
            //            return (Handler)Activator.CreateInstance(t);
            //    }
            //}

            if (typeName != null)
            {
                return new CustomHandler { Key = typeName, HandlerTypeName = handlerTypeName };
            }
            else
            {
                return new CustomHandler { Key = node.Name.ToString(), HandlerTypeName = handlerTypeName };
            }
        }

        private string GetTypeName(string fullName)
        {
            return fullName.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries)
                .First()
                .Split('.')
                .Last();
        }

        private void SetHandlerProperties(Handler handler, XElement node)
        {
            var properties = handler.GetType().GetProperties();

            foreach (var p in properties)
            {
                var attribute = node.Attribute(p.Name);

                if (attribute != null)
                    p.SetValue(handler, attribute.Value);
            }

            var editItem = node.Element("EditItem");

            if (editItem != null)
            {
                handler.CodeName = editItem.Attribute("CodeName").Value;
                handler.IsParametersVisible = Convert.ToBoolean(editItem.Attribute("IsParametersVisible").Value);
                handler.OnlyValueHandlersCanUsed = Convert.ToBoolean(editItem.Attribute("OnlyValueHandlersCanUsed").Value);

                var xmlParameters = editItem.Element("Parameters");

                if (xmlParameters != null)
                {
                    var parameters = new List<Parameter>();

                    foreach (var p in xmlParameters.Elements())
                    {
                        parameters.Add(new Parameter
                        {
                            Name = p.Attribute("Name").Value,
                            TypeName = p.Attribute("TypeName").Value,
                            Value = p.Attribute("Value").Value
                        });
                    }

                    handler.Parameters = parameters;
                }
            }
        }

        private string GetHandlerTypeNameAttribute(XElement node)
        {
            var editItem = node.Elements().FirstOrDefault();

            return editItem?.Attributes().TryGetAttributeValue("HandlerTypeName");
        }
    }
}