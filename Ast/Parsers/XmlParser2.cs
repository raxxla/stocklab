﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Ast.Extensions;

namespace Ast.Parsers
{
    public class XmlParser2 : IXmlParser
    {
        public IDictionary<string, Handler> GetHandlers(XDocument document)
        {
            var xmlNodes = GetXmlNodes(document);

            if (xmlNodes.Count() == 0)
                throw new Exception();

            var handlers = CreateHandlers(xmlNodes);

            CreatePanes(GetPanes(document)).ToList().ForEach(p => handlers.Add(p));

            return handlers;
        }

        public IEnumerable<XmlLink> GetLinks(XDocument document)
        {
            var xmlNodes = GetXmlNodes(document);

            if (xmlNodes.Count() == 0)
                throw new Exception();

            var result = new List<XmlLink>();

            foreach (var node in xmlNodes)
            {
                var links = node.Elements("Children").Elements("ItemName");

                foreach (var l in links)
                {
                    result.Add(new XmlLink
                    {
                        From = node.Attribute("Name").Value,
                        To = l.Value
                    });
                }
            }

            var panes = GetPanes(document);

            foreach (var pane in panes)
            {
                var links = pane.Element("GraphsList").Elements("Graph");

                foreach (var l in links)
                {
                    result.Add(new XmlLink
                    {
                        From = l.Attribute("SourceName").Value,
                        To = pane.Attribute("Name").Value
                    });
                }
            }

            return result;
        }

        private IEnumerable<XElement> GetXmlNodes(XDocument document)
        {
            return document.Element("GraphViewData")
                .Element("Template")
                .Element("Items")
                .Elements();
        }

		public IEnumerable<XElement> GetXmlMappingNodes(XDocument document)
		{
			return document.Element("GraphViewData")
				.Element("Mappings")
				.Element("Sources")
				.Elements();
		}

		private IEnumerable<XElement> GetPanes(XDocument document)
        {
            return document.Element("GraphViewData")
                .Element("Template")
                .Element("Panes")
                .Elements();
        }

        private IDictionary<string, Handler> CreateHandlers(IEnumerable<XElement> nodes)
        {
            var result = new Dictionary<string, Handler>();

            foreach (var xmlNode in nodes)
            {
                var handler = CreateHandler(xmlNode);
                SetHandlerProperties(handler, xmlNode);
                result.Add(handler.Key, handler);
            }

            return result;
        }

        private IDictionary<string, Handler> CreatePanes(IEnumerable<XElement> nodes)
        {
            var result = new Dictionary<string, Handler>();

            foreach (var xmlNode in nodes)
            {
                var handler = new CustomHandler { Key = xmlNode.Attribute("Name").Value };
                result.Add(handler.Key, handler);
            }

            return result;
        }

        private Handler CreateHandler(XElement node)
        {
            var handlerTypeName = GetHandlerTypeName(node);

            if (handlerTypeName != null)
            {
                handlerTypeName = GetTypeName(handlerTypeName);
                var types = Assembly.GetExecutingAssembly().GetTypes();

                foreach (var t in types)
                {
                    try
                    {
                        if (t.Name == handlerTypeName)
                            return (Handler)Activator.CreateInstance(t);
                    }
                    catch
                    {
                        return null;
                    }
                }
            }

            var key = node.Attributes().TryGetAttributeValue("type");

            if (key != null)
            {
                return new CustomHandler { Key = key };
            }
            else
            {
                return new CustomHandler { Key = node.Name.ToString() };
            }
        }

        private void SetHandlerProperties(Handler handler, XElement node)
        {
            handler.Key = node.Attribute("Name").Value;
            handler.Category = node.Attributes().TryGetAttributeValue("type");
            handler.CodeName = node.Attribute("CodeName").Value;

            var typeName = node.Element("HandlerTypeName");
            handler.TypeName = typeName != null ? typeName.Value : null;

            var center = node.Element("Center");
            handler.Location = center.Element("X").Value + " " + center.Element("Y").Value;
            handler.Compacted = Convert.ToBoolean(node.Element("Compacted").Value);

            var xmlParameters = node.Element("Parameters");

            if (xmlParameters != null)
            {
                var parameters = new List<Parameter>();

                foreach (var p in xmlParameters.Elements())
                {
                    parameters.Add(new Parameter
                    {
                        Name = p.Attribute("Name").Value,
                        TypeName = p.Attribute("TypeName").Value,
                        Value = p.Attribute("Value").Value
                    });
                }

                handler.Parameters = parameters;
            }

            var xmlInputs = node.Element("Inputs");

            if (xmlInputs != null)
            {
                var inputs = new List<string>();

                foreach (var i in xmlInputs.Elements())
                    inputs.Add(i.Value);

                handler.Inputs = inputs;
            }
        }

        private string GetHandlerTypeName(XElement node)
        {
            var typeName = node.Element("HandlerTypeName");
            return typeName != null ?
                typeName.Value :
                null;
        }

        private string GetTypeName(string fullName)
        {
            return fullName.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries)
                .First()
                .Split('.')
                .Last();
        }
    }
}
