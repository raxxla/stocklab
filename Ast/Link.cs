﻿namespace Ast
{
    public class Link
    {
        public Handler From { get; set; }
        public Handler To { get; set; }
        public string FromPort { get; set; }
        public string ToPort { get; set; }
        public int? ToPortNum { get; set; }

		public override string ToString() => $"{From} -> {To}";
	}
}