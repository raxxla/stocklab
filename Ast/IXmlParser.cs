﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Ast
{
    public interface IXmlParser
    {
        IDictionary<string, Handler> GetHandlers(XDocument document);
        IEnumerable<XmlLink> GetLinks(XDocument document);
		IEnumerable<XElement> GetXmlMappingNodes(XDocument document);
	}
}
