﻿namespace Ast
{
    public class XmlLink
    {
        public string From { get; set; }
        public string To { get; set; }
        public string FromPort { get; set; }
        public string ToPort { get; set; }
        public int? ToPortNum { get; set; }

		public override string ToString() => $"{From} -> {To}";

	}
}
