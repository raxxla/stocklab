﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;

namespace Ast
{
    public static class XmlParser
    {
        public static IXmlParser FindParser(string fileName, out XDocument document)
        {
            var parsers = GetParsers();

			document = XDocument.Load(fileName);

            foreach (var p in parsers)
            {
                try
                {
                    p.GetHandlers(document);
                    p.GetLinks(document);
                }
                catch
                {
                    continue;
                }

                return p;
            }

            throw new Exception("Parser not found");
        }

        private static IEnumerable<IXmlParser> GetParsers()
        {
            return Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.GetInterface("IXmlParser") != null)
                .ToList()
                .Select(t => (IXmlParser)Activator.CreateInstance(t));
        }
    }
}