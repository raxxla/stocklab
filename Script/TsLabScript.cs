﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using StockSharp.Algo;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Strategies;
using StockSharp.BusinessEntities;

namespace SampleQuik.Script
{
	public class TsLabScript
	{
		public int IndicatorCount;

		public readonly Ast.Handler[] Handlers;

		public List<Ast.Handler> UsedIndicators = new List<Ast.Handler>();
		public List<Ast.Handler> DataSources = new List<Ast.Handler>();

		public List<ParameterExpression> VarList = new List<ParameterExpression>();
		public List<Expression> BlockExpressions = new List<Expression>();
		public ParameterExpression paramSources = Expression.Parameter(typeof(Candle[]), "candles");
		public ParameterExpression paramIndicators = Expression.Parameter(typeof(IIndicator[]), "indicators");
		public ParameterExpression paramInstruments = Expression.Parameter(typeof(TsLabStrategy[]), "instruments");

		public List<ParameterExpression> VarList2 = new List<ParameterExpression>();
		public List<Expression> BlockExpressions2 = new List<Expression>();

		public BlockExpression Block;

		public ParameterExpression[] Parameters => new ParameterExpression[] { paramSources, paramIndicators, paramInstruments };

		public TsLabScript(Ast.Handler[] handlers)
		{
			Handlers = handlers;
		}

		public Ast.Handler FindDataSource(string key) => DataSources.FirstOrDefault(h => h.Key == key);

		public BlockExpression BuildClosePositionExpressions(Dictionary<string, CustomExpressionFunction> customMap)
		{
			foreach (var handler in Handlers)
			{
				if (handler.OutputLinks.Count() == 0)
					ProcessCloseHandler(handler, customMap);
			}

			return Expression.Block(typeof(void), VarList2, BlockExpressions2);
		}

		private void ProcessCloseHandler(Ast.Handler handler, Dictionary<string, CustomExpressionFunction> customMap)
		{
			var parents = handler.Parents.ToArray();

			foreach (var parent in parents)
				ProcessCloseHandler(parent, customMap);

			if ((handler.ValueRefExp ?? handler.ValueInitExp) != null)
				return;

			var typeName = handler.ShortTypeName ?? handler.TypeName;

			if (typeName == null)
				return;

			customMap.TryGetValue(typeName, out var custom);

			custom?.Invoke(handler, parents, this, VarList2, BlockExpressions2);
		}

		public static TsLabScript BuildOpenPositionExpressions(Ast.Handler[] handlers, Dictionary<string, CustomExpressionFunction> customMap)
		{
			var script = new TsLabScript(handlers);

			var arrayIndex = 0;

			foreach (var handler in handlers)
			{
				if (handler.Category == "SECURITY")
				{
					var v = Expression.Variable(typeof(Candle), handler.CodeName + "_candle");
					var arrayIndexExp = Expression.Constant(arrayIndex++);

					var vInit = v.Assign(Expression.ArrayAccess(script.paramSources, arrayIndexExp));

					script.VarList.Add(v);
					script.BlockExpressions.Add(vInit);

					handler.ValueRefExp = v;
					handler.ValueInitExp = vInit;

					handler.DataSourceIndex = script.DataSources.Count;

					script.DataSources.Add(handler);

					var v2 = Expression.Variable(typeof(TsLabStrategy), handler.CodeName + "_instrument");

					var v2Init = v2.Assign(Expression.ArrayAccess(script.paramInstruments, arrayIndexExp));

					script.VarList.Add(v2);
					script.BlockExpressions.Add(v2Init);

					handler.InstrumentRefExp = v2;
					handler.InstrumentInitExp = v2Init;

				}
			}

			foreach (var handler in handlers)
			{
				if (handler.OutputLinks.Count() == 0)
					script.ProcessOpenHandler(handler, customMap);
			}

			script.Block = Expression.Block(typeof(void), script.VarList, script.BlockExpressions);

			return script;
		}

		private void ProcessOpenHandler(Ast.Handler handler, Dictionary<string, CustomExpressionFunction> customMap)
		{
			var parents = handler.Parents.ToArray();

			foreach (var parent in parents)
				ProcessOpenHandler(parent, customMap);

			if ((handler.ValueRefExp ?? handler.ValueInitExp) != null)
				return;

			var typeName = handler.ShortTypeName ?? handler.TypeName;

			if (typeName == null)
				return;

			customMap.TryGetValue(typeName, out var custom);

			if (custom != null)
			{
				custom(handler, parents, this, VarList, BlockExpressions);

				return;
			}

			var handlerValueVar = Expression.Variable(typeof(decimal), handler.CodeName);
			Expression vInit = null;

			StockSharpIndicator.Map.TryGetValue(typeName, out var ssIndic);

			vInit = ssIndic?.ProcessHandler(handler, parents, this, ref handlerValueVar);

			TradeMath.SelectorMap.TryGetValue(typeName, out var selectExp);

			if (selectExp != null)
			{
				handler.Selector = selectExp.BindParameters(parents);

				vInit = Expression.Assign(handlerValueVar, selectExp.BindBodyParameters(parents[0].ValueRefExp));
			}

			TradeMath.MathMap.TryGetValue(typeName, out var buildTree);

			if (buildTree != null)
			{
				var varValue = buildTree(handler, parents);

				if(varValue.Type != handlerValueVar.Type)
					handlerValueVar = Expression.Variable(varValue.Type, handler.CodeName);

				vInit = Expression.Assign(handlerValueVar, buildTree(handler, parents));
			}

			if (vInit == null || handlerValueVar == null)
			{
				//var s0 = parents[0].ClosestSelector;

				return;
			}

			VarList.Add(handlerValueVar);
			BlockExpressions.Add(vInit);

			handler.ValueRefExp = handlerValueVar;
			handler.ValueInitExp = vInit;
		}

		public Dictionary<string, string> MapDataSources(Ast.IXmlParser parser, System.Xml.Linq.XDocument doc)
		{
			var result = new Dictionary<string, string>();

			var mapping = parser.GetXmlMappingNodes(doc);

			foreach (var map in mapping)
			{
				var srcName = map.Attribute("Name")?.Value;
				var secId = map.Attribute("SecurityId")?.Value;

				var hdl = FindDataSource(srcName);

				if (hdl != null)
				{
					hdl.Mapping = secId;

					result.Add(srcName, secId);
				}
			}

			return result;
		}

		public IIndicator[] CreateIndicators()
		{
			IIndicator[] result = new IIndicator[UsedIndicators.Count];

			for (int i = 0; i < result.Length; i++)
				result[i] = UsedIndicators[i].CreateIndicator();

			return result;
		}

	}

	public class TsLabStrategy : TimeFrameStrategy
	{
		public Dictionary<string, string> SourceNameToSecurityIdMap;

		private Portfolio _portfolio;

		public TsLabStrategy(Connector trader, TimeSpan time ):base(time)
		{
			base.Connector = trader;
		}

		protected override ProcessResults OnProcess()
		{
			return ProcessResults.Continue;
		}

		public void SetPortfolio(string name = null)
		{
			if (name == null)
				_portfolio = Connector.Portfolios.FirstOrDefault();
			else
				_portfolio = Connector.GetPortfolio(name);
		}

		public decimal UpdateTrailStop(decimal curPrice, decimal stopLoss, decimal trail, decimal loss)
		{
			var StopLoss = 1.5D;
			var TrailEnable = 0.5D;
			var TrailLoss = 0.5D;

			return 0;
			// TODO implement
		}

		public void UpdateCommission(double commission, double margin)
		{
			//SourceNameToSecurityIdMap?.TryGetValue(sourceName, out var securityId);

			// TODO implement
		}

	}

	public class VarFinder : ExpressionVisitor
	{
		private Type _varType;

		private ParameterExpression _result;

		public ParameterExpression FindVariable<T>(Expression expression)
		{
			_varType = typeof(T);

			Visit(expression);

			return _result;
		}

		protected override Expression VisitParameter(ParameterExpression node)
		{
			if (node.Type == _varType)
				_result = node;

			return base.VisitParameter(node);
		}
	}

	public class Modifier : ExpressionVisitor
	{
		public Func<ParameterExpression, Expression> ParamUpdater;

		public Expression Modify(Expression expression)
		{
			return Visit(expression);
		}

		protected override Expression VisitBinary(BinaryExpression b)
		{
			var left = Visit(b.Left);
			var right = Visit(b.Right);

			return Expression.MakeBinary(b.NodeType, left, right, b.IsLiftedToNull, b.Method);
		}

		protected override Expression VisitMember(MemberExpression node)
		{
			var expression = Visit(node.Expression);

			return Expression.MakeMemberAccess(expression, node.Member);
		}

		protected override Expression VisitMethodCall(MethodCallExpression node)
		{
			var obj = Visit(node.Object);

			List<Expression> args = new List<Expression>();

			foreach (var a in node.Arguments)
				args.Add(Visit(a));

			return Expression.Call(obj, node.Method, args);
		}

		protected override Expression VisitParameter(ParameterExpression node)
		{
			//var valRefExp = ParamExpression;

			//if (valRefExp.Type == node.Type)
			//	return valRefExp;

			return ParamUpdater?.Invoke(node) ?? node;
		}

		protected override Expression VisitLambda<T>(Expression<T> node)
		{
			return base.VisitLambda(node);
		}
	}

	public abstract class Exp
	{
		public static MethodCallExpression Strategy(Expression<Action<TsLabStrategy>> exp) => exp.Body as MethodCallExpression;

		public abstract Expression Declaration { get; }
		public abstract Expression Body { get; }

		public Expression MakeStrategyIndicatorCall(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script)
		{
			if (Body is MethodCallExpression call)
			{
				var args = new Expression[2];

				args[0] = handler.IndicRefExp;
				args[1] = parents[0].ValueRefExp;

				return Expression.Call(script.paramInstruments, call.Method, args);
			}

			return null;
		}

		public Expression MakeStaticCall(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script)
		{
			if (Body is MethodCallExpression call)
			{
				var args = new Expression[3];

				args[0] = handler.IndicRefExp;
				args[1] = parents[0].ValueRefExp;
				args[2] = Expression.Constant(true);

				return Expression.Call(null, call.Method, args);
			}

			return null;
		}
	}

	public class Exp<T> : Exp // where T : class
	{
		public readonly Expression<T> LambdaExpression;
		private T _lambda;

		public T Lambda => _lambda != null ? _lambda : _lambda = LambdaExpression.Compile();

		public override Expression Declaration => LambdaExpression;
		public override Expression Body => LambdaExpression.Body;

		public static MethodCallExpression Method(Expression<Action<T>> exp) => exp.Body as MethodCallExpression;
		public static MemberExpression Member(Expression<Func<T, object>> exp)
		{
			var result = exp.Body as MemberExpression;

			if (result != null)
				return result;

			var unary = exp.Body as UnaryExpression;

			return unary?.Operand as MemberExpression;
		}

		public Exp(Expression<T> exp) => LambdaExpression = exp;

		public Expression BindBodyParameters(Expression exp)
		{
			Func<ParameterExpression, Expression> updater = p => p.Type == exp.Type ? exp : p;

			var modifier = new Modifier() { ParamUpdater = updater };

			var result = modifier.Modify(Body);

			return result;
		}

		public Expression BindParameters(Ast.Handler[] parents)
		{
			var newExp = parents[0].ValueRefExp;

			Func<ParameterExpression, Expression> updater = p => p.Type == newExp.Type ? newExp : p;

			var modifier = new Modifier() { ParamUpdater = updater };

			var result = modifier.Modify(LambdaExpression);

			return result;
		}

		//public static implicit operator Exp<T>(Expression<T> exp) => new Exp<T>(exp);
	}

}
