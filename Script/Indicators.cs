﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using StockSharp.Algo.Candles;
using StockSharp.Algo.Indicators;

namespace SampleQuik.Script
{
	public delegate decimal Selector(Candle candle);
	public delegate decimal IndicatorAction(TsLabStrategy strategy, IIndicator indicator, decimal value);
	public delegate IIndicatorValue IndicatorHelperAction(IIndicator indicator, decimal value);

	public class StockSharpIndicator
	{
		public static Dictionary<string, StockSharpIndicator> Map = new Dictionary<string, StockSharpIndicator>();

		// declare indicator function call
		private static Exp<IndicatorAction> CallIndicator(Expression<IndicatorAction> exp) => new Exp<IndicatorAction>(exp);
		private static Exp<IndicatorHelperAction> CallIndicator(Expression<IndicatorHelperAction> exp) => new Exp<IndicatorHelperAction>(exp);

		//public static readonly Exp<IndicatorAction> SSharpInvoker = CallIndicator((strategy, indicator, value) => strategy.Update(indicator, value));
		public static readonly Exp<IndicatorHelperAction> SSharpInvoker = CallIndicator((indicator, value) => IndicatorHelper.Process(indicator, value, true));

		static StockSharpIndicator()
		{
			Register<SimpleMovingAverage>("SMA", SSharpInvoker);
			Register<SmoothedMovingAverage>("SMMA", SSharpInvoker);
			Register<ExponentialMovingAverage>("EMA", SSharpInvoker);
			Register<TripleExponentialMovingAverage>("TEMA", SSharpInvoker);
			Register("FAMA", SSharpInvoker);// TODO find closest indicator
			Register<DoubleExponentialMovingAverage>("DEMA", SSharpInvoker);
			Register("LWMA", SSharpInvoker);// TODO find closest indicator
			Register("AMA", SSharpInvoker);// TODO find closest indicator
			Register("MAMA", SSharpInvoker);// TODO find closest indicator
			Register<AverageDirectionalIndex>("ADX", SSharpInvoker);
			Register<AverageDirectionalIndex>("ADXFull", SSharpInvoker);
			Register("AroonDown", SSharpInvoker);// TODO find closest indicator
			Register("AroonUp", SSharpInvoker);// TODO find closest indicator
			Register<AverageTrueRange>("AverageTrueRange", SSharpInvoker);
			Register<TrueRange>("TrueRange", SSharpInvoker);
			Register<BollingerBand>("BollingerBands1", SSharpInvoker);
			Register<BollingerBands>("BollingerBands2", SSharpInvoker);
			Register<CommodityChannelIndex>("CCI", SSharpInvoker);
			Register<RelativeStrengthIndex>("CuttlerRSI", SSharpInvoker);
			Register<DiMinus>("DIM", SSharpInvoker);
			Register<DiPlus>("DIP", SSharpInvoker);
			Register<Highest>("Highest", SSharpInvoker);
			Register<Lowest>("Lowest", SSharpInvoker);
			Register<MovingAverageConvergenceDivergence>("MACD", SSharpInvoker);
			Register<MovingAverageConvergenceDivergenceHistogram>("MACDEx", SSharpInvoker);
			Register<MovingAverageConvergenceDivergenceSignal>("MACDSig", SSharpInvoker);
			Register<Momentum>("Momentum", SSharpInvoker);
			Register("MomentumOsc", SSharpInvoker);// TODO find closest indicator
			Register("MomentumPct", SSharpInvoker);// TODO find closest indicator
			Register<ParabolicSar>("ParabolicSAR", SSharpInvoker);
			Register("Relative", SSharpInvoker);// TODO find closest indicator
			Register<RelativeStrengthIndex>("RSI", SSharpInvoker);
			Register<StandardDeviation>("StDev", SSharpInvoker);
			Register<StochasticK>("StochK", SSharpInvoker);
			Register<StochasticOscillator>("StochRsi", SSharpInvoker);
			Register<Sum>("SummFor", SSharpInvoker);
			Register<Trix>("TRIX", SSharpInvoker);
			Register<ChaikinVolatility>("Volatility", SSharpInvoker);
			Register<TripleExponentialMovingAverage>("ZeroLagTEMA", SSharpInvoker);// TODO find closest indicator
		}

		public readonly Type SSharpType;
		public readonly Exp<IndicatorAction> UpdateIndicatorExpression;
		public readonly Exp<IndicatorHelperAction> IndicatorHelperExpression;

		public static void Register<I>(string name, Exp<IndicatorAction> exp, string tsLabNamespace = "TSLab.Script.Handlers") where I : BaseIndicator
		{
			Register(name, exp, typeof(I), tsLabNamespace);
		}

		public static void Register(string name, Exp<IndicatorAction> exp, Type type = null, string tsLabNamespace = "TSLab.Script.Handlers")
		{
			var handlerName = $"{tsLabNamespace}.{name}";
			var item = new StockSharpIndicator(exp, type);

			Map.Add(handlerName, item);
		}

		public static void Register<I>(string name, Exp<IndicatorHelperAction> exp, string tsLabNamespace = "TSLab.Script.Handlers") where I : BaseIndicator
		{
			Register(name, exp, typeof(I), tsLabNamespace);
		}

		public static void Register(string name, Exp<IndicatorHelperAction> exp, Type type = null, string tsLabNamespace = "TSLab.Script.Handlers")
		{
			var handlerName = $"{tsLabNamespace}.{name}";
			var item = new StockSharpIndicator(exp, type);

			Map.Add(handlerName, item);
		}

		public StockSharpIndicator(Exp<IndicatorAction> exp, Type type)
		{
			UpdateIndicatorExpression = exp;
			SSharpType = type;
		}
		public StockSharpIndicator(Exp<IndicatorHelperAction> exp, Type type)
		{
			IndicatorHelperExpression = exp;
			SSharpType = type;
		}

		public Expression ProcessHandler(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, ref ParameterExpression v)
		{
			if (UpdateIndicatorExpression != null)
			{
				if (handler.IndicRefExp == null)
				{
					v = Expression.Variable(typeof(decimal), v.Name + "_value");

					handler.SSharpIndicator = this;

					var indic = Expression.Variable(typeof(IIndicator), handler.CodeName);
					var arrayIndexExp = Expression.Constant(script.IndicatorCount++);
					var indicInit = indic.Assign(Expression.ArrayAccess(script.paramIndicators, arrayIndexExp));

					handler.IndicRefExp = indic;
					handler.IndicInitExp = indicInit;

					script.VarList.Add(indic);
					script.BlockExpressions.Add(indicInit);

					script.UsedIndicators.Add(handler);
				}

				handler.ValueRefExp = v;

				return Expression.Assign(v, UpdateIndicatorExpression.MakeStrategyIndicatorCall(handler, parents, script));
			}

			if (IndicatorHelperExpression != null)
			{
				if (handler.IndicRefExp == null)
				{
					v = Expression.Variable(typeof(IIndicatorValue), v.Name + "_value");

					handler.SSharpIndicator = this;

					var indic = Expression.Variable(typeof(IIndicator), handler.CodeName);
					var arrayIndexExp = Expression.Constant(script.IndicatorCount++);
					var indicInit = indic.Assign(Expression.ArrayAccess(script.paramIndicators, arrayIndexExp));

					handler.IndicRefExp = indic;
					handler.IndicInitExp = indicInit;

					script.VarList.Add(indic);
					script.BlockExpressions.Add(indicInit);

					script.UsedIndicators.Add(handler);
				}

				handler.ValueRefExp = v;

				return Expression.Assign(v, IndicatorHelperExpression.MakeStaticCall(handler, parents, script));
			}

			return null;
		}

		public IIndicator CreateInstance(Ast.Handler handler)
		{
			if (SSharpType == null)
				return null;

			var indicator = Activator.CreateInstance(SSharpType) as IIndicator;

			if(indicator is LengthIndicator<decimal> indic)
			{
				indic.Length = (int)handler.GetParameterValue(0, 12);
			}

			return indicator;
		}

		//public static decimal SMA(Candle source, Selector selector, int period)
		//{
		//	var barIndex = source.Candles.Count - 1 - barsAgo;

		//	var curBar = barIndex - period + 1;

		//	if (curBar < 0)
		//		curBar = 0;

		//	period = barIndex - curBar + 1;

		//	var value = 0.0M;

		//	while (curBar <= barIndex && curBar < source.Candles.Count)
		//		value += selector(source, curBar++);

		//	return value / Math.Min(period, barIndex + 1);
		//}

		//public static decimal EMA(ICandleSource source, Selector selector, int period, int barsAgo)
		//{
		//	var barIndex = source.Candles.Count - 1 - barsAgo;

		//	if (barIndex < period)
		//	{
		//		return SMA(source, selector, period, barsAgo);
		//	}
		//	else
		//	{
		//		var curVal = selector(source, barsAgo);
		//		var prevVal = selector(source, barsAgo + 1);
		//		var mul = 2.0M / (1.0M + period);

		//		return  mul * (curVal - prevVal) + prevVal;
		//	}
		//}
		//public static decimal EMA2(ICandleSource source, Func<ICandleSource, int, decimal> selector, int period, int barsAgo)
		//{
		//	var barIndex = source.Candles.Count - 1 - barsAgo;

		//	if (barIndex < period)
		//	{
		//		//return SMA(source, selector, period, barsAgo);

		//		return 0;
		//	}
		//	else
		//	{
		//		var curVal = selector(source, barsAgo);
		//		var prevVal = selector(source, barsAgo + 1);
		//		var mul = 2.0M / (1.0M + period);

		//		return mul * (curVal - prevVal) + prevVal;
		//	}
		//}
	}
}
