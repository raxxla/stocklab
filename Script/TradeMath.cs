﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

using StockSharp.Algo.Candles;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Strategies;

namespace SampleQuik.Script
{
	public delegate bool BoolFunction(Candle source, Selector value1, Selector value2);
	public delegate bool BoolIndicatorFunction(IIndicator source1, IIndicator source2);
	public delegate Expression CustomExpressionFunction(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, List<ParameterExpression> varList, List<Expression> block);

	public class TradeMath
	{
		public static Dictionary<string, Exp<Selector>> SelectorMap = new Dictionary<string, Exp<Selector>>();
		public static Dictionary<string, Func<Ast.Handler, Ast.Handler[], Expression>> MathMap = new Dictionary<string, Func<Ast.Handler, Ast.Handler[], Expression>>();
		public static Dictionary<string, CustomExpressionFunction> CustomMapOpenPosition = new Dictionary<string, CustomExpressionFunction>();
		public static Dictionary<string, CustomExpressionFunction> CustomMapClosePosition = new Dictionary<string, CustomExpressionFunction>();

		static TradeMath()
		{
			CustomMapOpenPosition.Add("OpenPositionByMarketItem", OpenPositionByMarket);
			//CustomMap.Add("TSLab.Script.Handlers.RelativeCommission", CallCommission);
			CustomMapClosePosition.Add("TSLab.Script.Handlers.TrailStop", HandleTrailStop);
			CustomMapClosePosition.Add("ClosePositionByStopItem", ClosePosition);

			SelectorMap.Add("TSLab.Script.Handlers.Volume", VolumeInvoker);
			SelectorMap.Add("TSLab.Script.Handlers.Close", CloseInvoker);
			SelectorMap.Add("TSLab.Script.Handlers.Low", LowInvoker);
			SelectorMap.Add("TSLab.Script.Handlers.High", HighInvoker);
			SelectorMap.Add("TSLab.Script.Handlers.TypicalPrice", TypicalPriceInvoker);
			SelectorMap.Add("TSLab.Script.Handlers.MedianPrice", MedianPriceInvoker);

			MathMap.Add("TSLab.Script.Handlers.Sub", (handler, parents) => MakeSub(parents));
			MathMap.Add("TSLab.Script.Handlers.Add", (handler, parents) => MakeAdd(parents));
			MathMap.Add("TSLab.Script.Handlers.Multiply", (handler, parents) => MakeMultiply(handler, parents));

			MathMap.Add("TSLab.Script.Handlers.CrossUnder", (handler, parents) => MakeCross(handler, parents, CrossUnderFunc));
			MathMap.Add("TSLab.Script.Handlers.CrossOver", (handler, parents) => MakeCross(handler, parents, CrossOverFunc));
		}

		private static Expression ClosePosition(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, List<ParameterExpression> varList, List<Expression> block)
		{
			if (parents?.Length < 2 || parents[0].InstrumentRefExp == null)
				return null;

			var eqExp = parents[1].ValueRefExp.Equal(parents[1].ClosestSelectorHandler.ValueRefExp);

			var methodCall = Exp.Strategy(s => s.ClosePosition(0));

			var args = new Expression[2];

			args[0] = parents[0].InstrumentRefExp;
			args[1] = Expression.Constant(0M);

			var callExp = Expression.Call(null, methodCall.Method, args);

			var condExp = Expression.IfThen(eqExp, callExp);

			block.Add(condExp);

			//handler.ValueRefExp = v;
			handler.ValueInitExp = callExp;

			return null;
		}

		private static Expression HandleTrailStop(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, List<ParameterExpression> varList, List<Expression> block)
		{
			if (parents?.Length < 1 || parents[0].InstrumentRefExp == null)
				return null;

			var candleHandler = parents[0].Parents[0];

			varList.Add(candleHandler.ValueRefExp as ParameterExpression);
			block.Add(candleHandler.ValueInitExp);

			var closePriceHandler = parents[0].ClosestSelectorHandler;

			varList.Add(closePriceHandler.ValueRefExp as ParameterExpression);
			block.Add(closePriceHandler.ValueInitExp);

			var methodCall = Exp.Strategy(s => s.UpdateTrailStop(0, 0, 0, 0));

			var args = new Expression[4];

			var stopLoss = handler.GetParameterValue(0, 1.5m);
			var trail = handler.GetParameterValue(1, 0.5m);
			var loss = handler.GetParameterValue(2, 0.5m);

			args[0] = closePriceHandler.ValueRefExp;
			args[1] = Expression.Constant(stopLoss);
			args[2] = Expression.Constant(trail);
			args[3] = Expression.Constant(loss);

			var callExp = Expression.Call(parents[0].InstrumentRefExp, methodCall.Method, args);

			var priceVarExp = Expression.Variable(typeof(decimal), handler.Key + "_price");

			var varInitExp = priceVarExp.Assign(callExp);

			varList.Add(parents[0].InstrumentRefExp as ParameterExpression);
			block.Add(parents[0].InstrumentInitExp);

			varList.Add(priceVarExp);
			block.Add(varInitExp);

			handler.ValueRefExp = priceVarExp;
			handler.ValueInitExp = varInitExp;

			return null;
		}

		private static Expression OpenPositionByMarket(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, List<ParameterExpression> varList, List<Expression> block)
		{
			if (parents?.Length < 2 || parents[0].InstrumentRefExp == null)
				return null;

			handler.InstrumentRefExp = parents[0].InstrumentRefExp;
			handler.InstrumentInitExp = parents[0].InstrumentInitExp;

			var methodCall = Exp.Strategy(s => s.BuyAtMarket(0));

			var shares = 1M;

			if (handler.Parameters.Count > 0)
				shares = (decimal)handler.Parameters[0].DoubleValue;

			var args = new Expression[2];

			//args[0] = Expression.Constant(handler.Key);
			//args[1] = Expression.Constant(handler.DataSourceName);
			args[0] = handler.InstrumentRefExp;
			args[1] = shares.Constant(typeof(decimal?));

			var exp = Expression.Call(null, methodCall.Method, args);

			var vInit = Expression.IfThen(parents[1].ValueRefExp, exp);

			block.Add(vInit);

			handler.ValueInitExp = vInit;

			return null;
		}

		private static Expression CallCommission(Ast.Handler handler, Ast.Handler[] parents, TsLabScript script, List<ParameterExpression> varList, List<Expression> block)
		{
			if (parents?.Length < 1 || parents[0].InstrumentRefExp == null)
				return null;

			var methodCall = Exp.Strategy(s => s.UpdateCommission(0, 0));

			var comm = handler.GetParameterValue(0, 0.05);
			var margin = handler.GetParameterValue(1, 10.0);

			var args = new Expression[2];

			args[0] = Expression.Constant(comm);
			args[1] = Expression.Constant(margin);

			var exp = Expression.Call(parents[0].InstrumentRefExp, methodCall.Method, args);

			block.Add(exp);

			return null;
		}

		private static Expression MakeCross(Ast.Handler handler, Ast.Handler[] parents, Exp<BoolIndicatorFunction> cross)
		{
			//var s0 = parents[0].ClosestSelector;
			//var s1 = parents[1].ClosestSelector;

			var newExp1 = parents[0].IndicRefExp;
			Func<ParameterExpression, Expression> updater1 = p => (p.Type == newExp1.Type && p.Name == "source1" ) ? newExp1 : p;

			var modifier1 = new Modifier() { ParamUpdater = updater1 };

			var result1 = modifier1.Modify(cross.Body);

			var newExp2 = parents[1].IndicRefExp;
			Func<ParameterExpression, Expression> updater2 = p => (p.Type == newExp1.Type && p.Name == "source2") ? newExp2 : p;

			var modifier2 = new Modifier() { ParamUpdater = updater2 };

			var result2 = modifier2.Modify(result1);

			return result2;
		}

		private static Expression MakeMultiply(Ast.Handler handler, Ast.Handler[] parents)
		{
			var arg = handler.Parameters[0];

			return Expression.Multiply(parents[0].ValueRefExp, Expression.Constant(arg.DecimalValue));
		}

		//public static bool CrossUnder(IIndicator source1, IIndicator source2) => source1.GetValue(0) < source2.GetValue(0) && source1.GetValue(1) >= source2.GetValue(1);
		//public static bool CrossOver(IIndicator source1, IIndicator source2) => source1.GetValue(0) > source2.GetValue(0) && source1.GetValue(1) <= source2.GetValue(1);

		private static Expression MakeAdd(Ast.Handler[] parents)
		{
			if ((parents?.Length ?? 0) == 0)
				return null;

			if (parents.Length == 1)
				return Expression.Increment(parents[0].ValueRefExp);

			var result = Expression.Add(parents[0].ValueRefExp, parents[1].ValueRefExp);

			for (int i = 2; i < parents.Length; i++)
			{
				result = Expression.Add(result, parents[i].ValueRefExp);
			}

			return result;
		}

		private static Expression MakeSub(Ast.Handler[] parents)
		{
			if ((parents?.Length ?? 0) == 0)
				return null;

			if (parents.Length == 1)
				return Expression.Decrement(parents[0].ValueRefExp);

			var result = Expression.Subtract(parents[0].ValueRefExp, parents[1].ValueRefExp);

			return result;
		}

		// declare selector function call
		private static Exp<Selector> Select(Expression<Selector> exp) => new Exp<Selector>(exp);
		private static Exp<BoolFunction> Select(Expression<BoolFunction> exp) => new Exp<BoolFunction>(exp);
		private static Exp<BoolIndicatorFunction> Select(Expression<BoolIndicatorFunction> exp) => new Exp<BoolIndicatorFunction>(exp);
		private static MemberExpression Member<T>(Expression<Func<T, object>> exp) => exp.Body as MemberExpression;

		public static readonly Exp<BoolIndicatorFunction> CrossUnderFunc = Select( (source1, source2) => source1.GetValue(0) < source2.GetValue(0) && source1.GetValue(1) >= source2.GetValue(1));
		public static readonly Exp<BoolIndicatorFunction> CrossOverFunc = Select((source1, source2) => source1.GetValue(0) > source2.GetValue(0) && source1.GetValue(1) <= source2.GetValue(1));

		//public static readonly Exp<BoolFunction> CrossUnder = Select( (source, value1, value2, barsAgo) => value1(source, barsAgo) < value2(source, barsAgo) && value1(source, barsAgo + 1) >= value2(source, barsAgo + 1));
		//public static readonly Exp<BoolFunction> CrossOver = Select((source, value1, value2, barsAgo) => value1(source, barsAgo) > value2(source, barsAgo) && value1(source, barsAgo + 1) <= value2(source, barsAgo + 1));

		public static readonly Exp<Selector> VolumeInvoker = Select((candle) => candle.TotalVolume);

		public static readonly Exp<Selector> LowInvoker = Select((candle) => candle.LowPrice);

		public static readonly Exp<Selector> HighInvoker = Select((candle) => candle.HighPrice);

		public static readonly Exp<Selector> OpenInvoker = Select((candle) => candle.OpenPrice);

		public static readonly Exp<Selector> CloseInvoker = Select((candle) => candle.ClosePrice);

		public static readonly Exp<Selector> TypicalPriceInvoker = Select((candle) => (candle.LowPrice + candle.HighPrice + candle.ClosePrice) / 3M);

		public static readonly Exp<Selector> MedianPriceInvoker = Select((candle) => (candle.LowPrice + candle.HighPrice) / 2M);

	}
}